# MyAdmin
一个基于SpringBoot2 + Ant-Design-Pro 的管理后台系统,包含了用户管理，组织机构管理，角色管理，功能点管理，菜单管理，权限分配，数据权限分配，代码生成等功能

系统基于Spring Boot2技术，前端采用了Ant-Design-Pro。数据库以MySQL为实例，理论上是跨数据库平台.

基本技术栈来源于电子工业出版社的[<<Spring Boot 2 精髓 >>](http://ibeetl.com/sb2/#more) 

当前版本:1.0.0

技术交流群：726557298

开源地址：https://gitee.com/lukezhou/MyAdmin


# 1 使用说明

## 1.1 安装说明

从Git上获取代码后，通过IDE导入此Maven工程，包含俩个子工程

* myAdmin-core  ，核心包，包含了缓存，数据权限，公用的JS和HTML页面。
* myAdmin-system, 系统管理功能，包含了元数据、用户，组织机构，角色，权限，数据权限，代码生成等管理功能

com.hnico.myadmin.SystemApplication 是系统启动类.
在运行这个之前，还需要初始化数据库，位于docs/db/starter-mysql.sql,目前只提供mysql脚本。理论上支持所有数据库

还需要修改SpringBoot配置文件application.properties,修改你的数据库地址和访问用户

~~~properties
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/starter?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8&useSSL=false
spring.datasource.username=root
spring.datasource.password=123456

~~~

>
> 本系统基于Spring Boot 2 ，因此请务必使用JDK8，且打开编译选项[parameters](http://www.mamicode.com/info-detail-2162647.html),<u> 并重新编译工程，如果你没有使用Java8的 parameters 特性，系统不能正常使用</u>
如果成功启动后运行报错：变量userId未定义，位于第6行，那是因为你没有启用[parameters](http://www.mamicode.com/info-detail-2162647.html)

运行SystemApplication，然后访问http://127.0.0.1:8080/  输入admin/123456 则可以直接登录进入管理系统


## 1.2 创建子系统

MyAdmin 是一个适合大系统拆分成小系统的架构，或者是一个微服务系统，因此，如果你需要创建自己的业务系统.
建议你不要在MyAdmin添加代码，应该是新建立一个maven工程，依赖myAdmin-core，或者依赖myAdmin-system（如果你有后台管理需求，通常都有，但不是必须的）


# 感谢
[SpringBoot-plus](https://gitee.com/xiandafu/springboot-plus)



