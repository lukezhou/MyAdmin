package com.hnico.myadmin.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.WebApplicationInitializer;

/**
 * 
 * @author zhouyl
 *
 */

@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages= {"com.hnico.myadmin","com.ibeetl.admin"})
public class CoreApplication  extends SpringBootServletInitializer implements WebApplicationInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CoreApplication.class, args);
	}

}
