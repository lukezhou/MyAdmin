package com.hnico.myadmin.core.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hnico.myadmin.core.entity.User;

@Mapper
public interface UserMapper extends BaseMapper<User>  {

}
