package com.hnico.myadmin.core.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hnico.myadmin.core.dao.UserMapper;
import com.hnico.myadmin.core.entity.User;
import com.hnico.myadmin.core.service.UserService;

@Controller
public class HomeController {

	@Autowired
	UserMapper userMapper;
	
	@RequestMapping(value = "/")
	public String index() {
		
		int result = 0;
		// 初始化 User 对象
		User user = new User();

		// 插入 User (插入成功会自动回写主键到实体类)
		user.setName("Tom");
		user.setId("122112212");
		result = userMapper.insert(user);

		// 更新 User
		user.setSex(0);
		result = userMapper.updateById(user);

		// 查询 User
		User exampleUser = userMapper.selectById(user.getId());

		// 查询姓名为‘张三’的所有用户记录
		List<User> userList = userMapper.selectList(
		        new EntityWrapper<User>().eq("name", "张三")
		);

		// 删除 User
		result = userMapper.deleteById(user.getId());
		
		return "index";
	}
}
